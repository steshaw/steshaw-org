# steshaw.org

[![Circle CI](https://circleci.com/gh/steshaw/steshaw.org.svg?style=svg)](https://circleci.com/gh/steshaw/steshaw.org)

Source for my homepage at <http://steshaw.org>.


## Deploying locally (i.e. without CircleCI)

Use `bin/publish` to build and publish the site to GitHub Pages.
