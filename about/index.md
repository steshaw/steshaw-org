---
layout: page
title: About
---

<header class="page-header">
# Steve's Backstory
</header>

Born English. Grew up Australian — got the accent to prove it. Dual citizen.
Enjoys living in beautiful Brisbane but does still feel the tug of Ol'
Blighty — particularly my home town near Nottingham and Scotland.

I'm an avid computer programmer who loves programming languages. As an
industry veteran of more than 25 years, I've used an multitude of languages:
Ingres 4GL (don't ask),
[C](https://en.wikipedia.org/wiki/C_%28programming_language%29),
[Bourne Shell](https://en.wikipedia.org/wiki/Bourne_shell),
[Korn Shell](http://kornshell.org/),
[AWK](https://en.wikipedia.org/wiki/AWK),
[Perl 4](https://www.perl.org/),
[Delphi Object Pascal](http://delphi.wikia.com/wiki/Object_Pascal),
[C++](http://www.stroustrup.com/C++.html),
[Python](https://www.python.org/),
[Java](https://en.wikipedia.org/wiki/Java_%28programming_language%29),
[Ruby](https://www.ruby-lang.org/),
[JavaScript](https://developer.mozilla.org/en-US/docs/Web/javascript),
[C#](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/),
[Scala](https://scala-lang.org/),
and now—finally—[Haskell](https://www.haskell.org/).

While I was using those languages in industry, I was learning about all sorts
of other languages:
[Component Pascal](https://en.wikipedia.org/wiki/Component_Pascal),
[Oberon](https://en.wikipedia.org/wiki/Oberon_(programming_language)),
[Oberon-2](https://en.wikipedia.org/wiki/Oberon-2),
[Modula-3](https://en.wikipedia.org/wiki/Modula-3),
[Mesa/Cedar](https://en.wikipedia.org/wiki/Mesa_(programming_language)),
[ALGOL](https://en.wikipedia.org/wiki/ALGOL),
[Eiffel](https://en.wikipedia.org/wiki/Eiffel_(programming_language)),
[Sather](https://www1.icsi.berkeley.edu/~sather/),
[Icon](https://www.cs.arizona.edu/icon),
[CLU](https://en.wikipedia.org/wiki/CLU_(programming_language)),
[Smalltalk](https://en.wikipedia.org/wiki/Smalltalk),
[Scheme](https://schemers.org/),
[Goo](https://googoogaga.github.io/),
[Dylan](https://opendylan.org/),
[EuLisp](https://en.wikipedia.org/wiki/EuLisp),
[Racket](https://racket-lang.org/),
[Common Lisp](https://en.wikipedia.org/wiki/Common_Lisp),
[Shen](http://www.shenlanguage.org/),
[Slate](https://web.archive.org/web/20160313043048/http://slatelanguage.org/),
[Io](https://iolanguage.org/), [Factor](https://factorcode.org/),
[Objective-C](https://en.wikipedia.org/wiki/Objective-C),
[APL](https://en.wikipedia.org/wiki/APL_(programming_language)),
[J](https://en.wikipedia.org/wiki/J_(programming_language)),
[K](https://en.wikipedia.org/wiki/K_(programming_language)),
[Q](https://en.wikipedia.org/wiki/Q_(programming_language_from_Kx_Systems)),
[Prolog](https://en.wikipedia.org/wiki/Prolog),
[Caml](https://caml.inria.fr/),
[OCaml](https://ocaml.org/),
[F#](https://fsharp.org/),
[Standard ML](http://sml-family.org/),
[Mythryl](https://mythryl.org/),
[Mozart Oz](https://mozart.github.io/),
[Alice ML](https://www.ps.uni-saarland.de/alice/),
[Erlang](https://www.erlang.org/),
[Haskell](https://www.haskell.org/),
[Idris](http://idris-lang.org),
[Coq](https://coq.inria.fr/),
[Agda](http://wiki.portal.chalmers.se/agda/pmwiki.php),
[Ur](http://www.impredicative.com/ur/),
[ATS](http://www.ats-lang.org/),
[F*](http://www.ats-lang.org/),
[Haxe](https://haxe.org/),
[Elm](https://elm-lang.org/),
[TypeScript](http://typescript.org/),
[PureScript](http://www.purescript.org/),
[Swift](https://swift.org/),
[Rust](https://www.rust-lang.org/).
Of course, my depth of study varied greatly. In fact, most of these languages
I have learnt only superficially, often from research papers or books alone.

Thankfully, I'm working primarily in [Haskell](https://www.haskell.org/) at
the moment. I also like [Idris](http://idris-lang.org),
[PureScript](http://www.purescript.org/), and
[Scala](https://scala-lang.org/). I'd definitely take on
[OCaml](https://ocaml.org/) too, given the opportunity. I've more-or-less
given up using [Scala](https://scala-lang.org/)—though I still follow along
with developments from time to time. I haven't really gotten into
[PureScript](http://www.purescript.org/) as yet, though it looks enticing.
Unfortunately, [Idris](http://idris-lang.org) isn't ready for prime time, but
I'm hopeful that this could change with the advent of [Idris
2.0](https://github.com/edwinb/Blodwen/), and if a production-ready
runtime-system materialises. I've done a bit of introductory work with
[Coq](https://coq.inria.fr/) and
[Agda](http://wiki.portal.chalmers.se/agda/pmwiki.php) and have found it
quite challenging. Wish I had more time to try out
[Rust](https://www.rust-lang.org/) for systems programming or gamedev.


### Formative Years

<div class="c64 pull-right">
![](../images/C64_startup_animiert.gif)
</div>

I enjoyed programming for almost a decade prior to joining industry. I had
heaps of fun starting with Basic on the [Dick Smith
Wizard](http://ultimateconsoledatabase.com/others/dick_smith_wizzard.htm) in
[82](http://www.youtube.com/watch?v=JbCr15KkBxY). By the time I got a
[Commodore 64](http://en.wikipedia.org/wiki/Commodore_64), I was writing
text-based adventures (aka "go north; get chalice") in Basic, 2D sprite games
in 6502 assembly, and experimenting with electronic music composition. I
regret never upgrading to a [Commodore
Amiga](https://en.wikipedia.org/wiki/Amiga) as it seemed like an awesome
platform for writing [video
games](https://www.youtube.com/watch?v=rsuWgLEQBxM),
[demos](https://youtu.be/3wu8cnIpdLY?list=PL7C791DD55914C154) and
[compilers](http://strlen.com/amiga-e). I didn't own a PC until _after_
university. While I was studying, I was fond of the terminal labs. At the
time, I was quite partial to [VAX/VMS](http://en.wikipedia.org/wiki/OpenVMS)
and it's [DCL](http://en.wikipedia.org/wiki/DIGITAL_Command_Language) shell
scripting language until I was introduced to UNIX with it's Bourne and
eventually Korn shells.

### Interests

  - Programming languages: Programming Language Theory (PLT), TLC --- Type
    Theory, Logic (i.e. Proof Theory), and Category Theory; Compilers and
    Runtime Systems; Program Verification and Mechanised Metatheory.
  - Application Development: trade-offs, tricks, patterns, the big-picture, dogma-slaying, and
    philosophy with coffee.
  - Systems:
    - Operating Systems and Runtime systems---very similar beasts!
    - Distributed systems: cloud, web, messaging, middleware, and highly-scalable servers.
    - Database systems---SQL, NoSQL, NewSQL, and DB APIs/DSLs (e.g. LINQ).
    - Build systems:
      [GNU Make](https://www.gnu.org/software/make/),
      [Cook](http://miller.emu.id.au/pmiller/software/cook/),
      [SCons](http://www.scons.org/),
      [Waf](https://waf.io/),
      [Vesta](http://www.vestasys.org/),
      [Rake](https://github.com/ruby/rake),
      [Ant](https://ant.apache.org/),
      [Maven](https://maven.apache.org/),
      [Ivy](https://ant.apache.org/ivy/),
      [SBT](https://www.scala-sbt.org/),
      [Cabal](https://www.haskell.org/cabal/),
      [CMake](https://cmake.org/),
      [Nix](https://nixos.org/nix/),
      [Shake](https://shakebuild.com/),
      [Tup](http://gittup.org/tup/),
      [Ninja](https://ninja-build.org/),
      [Redo](https://cr.yp.to/redo.html),
      [Pants](https://www.pantsbuild.org/),
      [Buck](https://buckbuild.com/),
      [Bazel](https://bazel.build/),
      [Please](https://please.build/).
      My interest was originally
      piqued by Peter Miller's [Recursive Make Considered
      Harmful](http://aegis.sourceforge.net/auug97.pdf). My interest has
      rekindled recently with the publication of
      [Build systems à la carte](https://github.com/snowleopard/build).
    - Version Control Systems:
      [SCSS](https://en.wikipedia.org/wiki/Source_Code_Control_System),
      [RCS](https://www.gnu.org/software/rcs/),
      [CVS](https://www.nongnu.org/cvs/),
      [Subversion](https://subversion.apache.org/),
      [Aegis](http://aegis.sourceforge.net/),
      [arch](https://www.gnu.org/software/gnu-arch/),
      [Vesta](http://www.vestasys.org/),
      [Darcs](http://darcs.net/),
      [Mercurial](https://mercurial.selenic.com/),
      [Bazaar](https://bazaar.canonical.com/),
      [Git](https://git-scm.com/),
      [Veracity](http://veracity-scm.com/) (with integrated bug tracking and wiki),
      [Fossil](https://fossil-scm.org/) (with integrated bug tracking, wiki
      and technical notes).
      After much preferring Mercurial, I've become proficient with Git.
      However, I now have hopes that [Pijul](https://pijul.org/)—which
      sprung from Darcs but rewritten in Rust with better performance—will
      eventually offer a nicer DX with a sound model.

### Hobbies

  - Human languages: French, Spanish, and—once-up-a-time—Japanese.
  - Chess
