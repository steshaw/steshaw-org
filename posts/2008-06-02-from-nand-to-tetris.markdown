---
author: steshaw
comments: true
date: 2008-06-02 17:00:26+00:00
layout: post
slug: from-nand-to-tetris
title: From nand to tetris
wordpress_id: 82
categories: Programming
tags: Compilers, Computer Architecture, Computer Hardware, Operating Systems, Programming Languages
---

This looks like a great course+book: [The elements of computing systems](http://www.idc.ac.il/tecs/).

![The Elements of Computing Systems](http://www.nand2tetris.org/imgs/cover.jpg "The book")
