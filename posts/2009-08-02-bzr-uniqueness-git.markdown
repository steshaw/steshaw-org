---
author: steshaw
comments: true
date: 2009-08-02 00:32:42+00:00
layout: post
slug: bzr-uniqueness-git
title: Bzr uniqueness + Git
wordpress_id: 73
categories: Programming
tags: Bazaar, DVCS, Git, Mercurial
---

The unique thing about Bzr (compared with say Mercurial) seems to be that the community has developed tools for [some common workflows](http://bazaar-vcs.org/Workflows).

I'm still using Mercurial for my personal files at home. Since I use Linux as my primary development platform, Git is getting the upper hand for me. I'll have to give hg2git a try (this will give me the opportunity to finally prune my /Photos directory).  The git-svn support is useful when clients are using SVN and you want to work from your laptop. Also the [GitHub](http://github.com/) is pretty cool. I've [only played around with it](http://github.com/steshaw/) but it has paid accounts that could be used for off-site backup and for possibly for source code delivery to clients. I hear that Git works great on Mac but last I heard the support on Windows was still lacking. Git works in cygwin with a native port on the way. I haven't used Windows for development for a while but always used to install cygwin when I did. However, that's not necessarily a great solution for regular Windows/.NET guys. If I was doing a .NET project, I'd probably stick with Subversion - git-svn is always there for disconnected development.

I wonder if there are bzr style workflow solutions for Git...
